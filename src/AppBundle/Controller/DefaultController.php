<?php

namespace AppBundle\Controller;

/**
 * Les importations 
 */
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Film;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Image;
use AppBundle\Entity\Personne;
use AppBundle\Entity\Pays;
use AppBundle\Entity\Role;

use \Symfony\Component\Form\Extension\Core\Type\TextType;
use \Symfony\Component\Form\Extension\Core\Type\UrlType;
use \Symfony\Component\Form\Extension\Core\Type\RadioType;
use \Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use \Symfony\Component\Form\Extension\Core\Type\IntegerType;
use \Symfony\Component\Form\Extension\Core\Type\TextareaType;
use \Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \Symfony\Bridge\Doctrine\Form\Type\EntityType;

// Classe DefaultController qui hérite de la classe Controller
class DefaultController extends Controller
{
    /**
     * @Route("/index", name="homepage")
     * Fonction pour la page d'accueil
     */
    public function indexAction()
    {        
        return $this->render('@App/Default/index.html.twig', array());; // Retourne la vue de la page d'accueil
    }    
    
    /**
     * @Route("/recherche", name="search")
     * Fonction pour la recherche d'un film
     */
    public function rechercheAction (){
        return $this->render('@App/Film/recherche.html.twig', array());
    }
    
}
