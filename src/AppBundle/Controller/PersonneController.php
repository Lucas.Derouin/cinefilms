<?php

namespace AppBundle\Controller;

/**
 * Les importations 
 */
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Film;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Image;
use AppBundle\Entity\Personne;
use AppBundle\Entity\Pays;
use AppBundle\Entity\Role;
use AppBundle\Entity\PersonneFilm;

use \Symfony\Component\Form\Extension\Core\Type\TextType;
use \Symfony\Component\Form\Extension\Core\Type\UrlType;
use \Symfony\Component\Form\Extension\Core\Type\RadioType;
use \Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use \Symfony\Component\Form\Extension\Core\Type\IntegerType;
use \Symfony\Component\Form\Extension\Core\Type\TextareaType;
use \Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PersonneController extends Controller
{
  
    /**
     * @Route("/ajoutpers", name="addpers")
     * @Route("modifpers/{id}", name="modifpers")
     * Fonction pour ajouter une personne dans la BDD
     */
    public function ajoutpersonneAction(Personne $addpersonne = null, Request $request) {
        // +Objet personne
        if (!$addpersonne){
            $addpersonne = new Personne();
        }

        //FormBuilder
        $formBuilder = $this ->createFormBuilder($addpersonne)
                     ->add('nom')
                     ->add('prenom')
                     ->add('image', UrlType::class)
                     ->add('role', EntityType::class, [
                         'class' => Role::class,
                         'choice_label' => 'metier'
                     ])
                     ->add('nationalite', EntityType::class, [
                         'class' => Pays::class,
                         'choice_label' => 'nation'
                     ]);
          
        
        //FormBuilder generation
        $form = $formBuilder->getForm();
        
        if ($request->isMethod('POST')) { // Verifie la requête 
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($addpersonne);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add('Info', 'Personne enregistee');
              
                return $this->render('@App/Default/index.html.twig');
            }
        }

        //Appel la vue avec le formulaire en parametre
        return $this->render('@App/Personne/ajoutpersonne.html.twig', [
            'form' => $form -> CreateView(),
            'edit' => $addpersonne -> getId() != null
        ]);
      
    }
}
