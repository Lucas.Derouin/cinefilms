<?php

namespace AppBundle\Controller;

/**
 * Les importations 
 */
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Film;
use AppBundle\Entity\Personne;
use AppBundle\Entity\PersonneFilm;

use \Symfony\Bridge\Doctrine\Form\Type\EntityType;
use \Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PersonneFilmController extends Controller
{
  
    /**
     * @Route("/ajoutpersonnefilm", name="addpersmovie")
     * @Route("/modifpersonnefilm/{id}", name="editpersmovie")
     * Fonction pour ajouter un des personnes pour un film
     */
    public function ajoutPersFilmAction(PersonneFilm $personneFilm = null,Request $request,  ObjectManager $manager){
        $em = $this->getDoctrine()->getManager();
    
      if(!$personneFilm)  {
          $personneFilm = new PersonneFilm();
      }
      
        //FormBuilder
        $formBuilder = $this ->createFormBuilder($personneFilm)
                     ->add('personne', EntityType::class, [
                         'class' => Personne::class,                      
                         'choice_label' => 'nom'
                     ])
                     ->add('film', EntityType::class, [
                         'class' => Film::class,
                         'choice_label' => 'titre'
                     ]);
    
        //FormBuilder generation
        $form = $formBuilder->getForm();
    
        if ($request->isMethod('POST')) { //Verifie la requête
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($personneFilm);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add('Info', 'Personne associée');
                return $this->redirectToRoute('homepage');
                           
            }
        }

        //Appel la vue avec le formulaire en parametre
        return $this->render('@App/Film/ajoutpersonnefilm.html.twig', [
            'form' => $form -> CreateView(),
            'edit' => $personneFilm -> getId() != null
        ]);    
    }
}
