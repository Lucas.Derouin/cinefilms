<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Entity\Utilisateur;
use AppBundle\Form\UtilisateurType;
use AppBundle\Controller\DefaultController;

class SecuriteController extends Controller
{
  
    /**
     * @Route("/enregistrement", name="enregistrement")
     */
    public function enregistrementAction(Request $request, ObjectManager $manager,UserPasswordEncoderInterface $encoder) {
        $utilisateur = new Utilisateur();
    
        $form = $this->createForm(UtilisateurType::class, $utilisateur); 
    
        $form->handleRequest($request);
    
        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($utilisateur, $utilisateur->getPassword());
      
            $utilisateur->setPassword($hash);
      
            $manager->persist($utilisateur);
            $manager->flush();
      
            return $this->redirectToRoute('homepage');
        }
    
        return $this->render('@App/Securite/enregistrement.html.twig', [
            'form' => $form->createView()
        ]);    
    }
  
    /**
     * @Route("/connexion", name="connexion")
     */
    public function loginAction(){
    
        return $this->render('@App/Securite/login.html.twig');
    }
  
    /**
     * @Route("/deconnexion", name="deconnexion")
     */
    public function logoutAction(){    
    
    }
}
