<?php

namespace AppBundle\Controller;

/**
 * Les importations 
 */
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Film;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Image;
use AppBundle\Entity\Personne;
use AppBundle\Entity\Pays;
use AppBundle\Entity\Role;
use AppBundle\Entity\Commentaire;
use AppBundle\Entity\Utilisateur;

use AppBundle\Form\CommentaireType;
use AppBundle\Form\UtilisateurType;

use \Symfony\Component\Form\Extension\Core\Type\TextType;
use \Symfony\Component\Form\Extension\Core\Type\UrlType;
use \Symfony\Component\Form\Extension\Core\Type\RadioType;
use \Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use \Symfony\Component\Form\Extension\Core\Type\IntegerType;
use \Symfony\Component\Form\Extension\Core\Type\TextareaType;
use \Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FilmController extends Controller
{
    
    /**
     * @Route("/film/{id}", name="film")
     * Fonction pour l'affichage d'un film 
     */
    public function filmAction ($id,  Request $request, ObjectManager $manager){
        $em = $this->getDoctrine()->getManager();
        
        $film = new Film();
        $film = $em ->find('AppBundle:Film', $id);
        
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        
        $utilisateur = $this->getUser();
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $commentaire->setDateCreation(new \DateTime())
                ->setFilm($film)
                ->setUtilisateur($utilisateur);
            
            $manager->persist($commentaire);
            $manager->flush();
            
            return $this->redirectToRoute('film', ['id'=> $film->getId()]);
        }
        
        return $this->render('@App/Film/film.html.twig', [
            'film' => $film,
            'commentaireForm'=> $form->createView()
        ]);
    }
    
    /**
     * @Route("/ajoutfilm", name="addmovie")
     * @Route("/modiffilm/{id}", name="editfilm")
     * Fonction pour ajouter un film dans la BDD
     */
    public function ajoutfilmAction(Film $addfilm = null, Request $request) {
        //+Object film
        if (!$addfilm){
            $addfilm = new Film();
        }
        
        //FormBuilder
        $formBuilder = $this ->createFormBuilder($addfilm)
                     ->add('titre')
                     ->add('genre', EntityType::class, [
                         'class' => Genre::class,
                         'choice_label' => 'type'
                     ])
                     ->add('pays', EntityType::class, [
                         'class' => Pays::class,
                         'choice_label' => 'nation'
                     ])
                     ->add('annee', TextType::class)
                     ->add('duree', TextType::class)                    
                     ->add('synopsis')
                     ->add('image', UrlType::class);
        
        //FormBuilder generation
        $form = $formBuilder->getForm();
        
        if ($request->isMethod('POST')) { //Verifie la requête
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($addfilm);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add('Info', 'Film enregiste');
                
                return $this->redirectToRoute('film', ['id' => $addfilm->getId()]);                
            }
        }
        
        //Appel la vue avec le formulaire en parametre
        return $this->render('@App/Film/ajoutfilm.html.twig', [
            'form' => $form -> CreateView(),
            'edit' => $addfilm -> getId() != null
        ]);
        
    }
    
    /**
     * @Route("/liste", name="list")
     * Fonction pour le listage des films
     */
    public function listeAction (){    
        $em = $this->getDoctrine()->getManager();
        $lesfilms  = $em->getRepository('AppBundle:Film')->findall();
        return $this->render('@App/Film/liste.html.twig', array('lesfilms' => $lesfilms)); // Retourne la vue de la page d'accueil
    }
}
