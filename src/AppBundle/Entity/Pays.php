<?php
//Class Pays.php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Pays")
 */
class Pays {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nation", type="string")
     */
    private $nation;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Film", mappedBy="pays")
     */
    private $filmpays;
  
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Personne", mappedBy="nationalite")
     */
    private $personnepays;
    
    //Accesseurs
  
    //Getters et setters pour l'objet Id
    function getId(){
        return $this->id; //Retourne en lecture l'objet Id
    }
    function setId($id){
        $this->id = $id; //Permet de determiner une valeur pour l'objet Id
    }

    //Getters et setters pour l'objet Nation
    function getNation(){
        return $this->nation; //Retourne en lecture l'objet Nation
    }
    function setNation($nation){
        $this->nation = $nation; //Permet de determiner une valeur pour l'objet Nation
    }
  
    //Methodes
  
    public function _construct() // Fonction lorsqu'il construit un film
    {
        $this ->filmpays = new ArrayCollection();
    }
  
    public function addFilmPays(Film $film): self //Fonction ajout Film qui permet d'ajouter un film dans un genre
    {
        if (!$this->filmpays->contains($film)) {
            $this->filmpays[] = $film;
            $film->setPays($film);
        }
        return $this;
    }
  
    public function removeFilmPays(Film $film): self //Fonction supprimer Film qui permet de supprimer un film dans un genre
    {
        if (!$this->filmpays->contains($film)) {
            $this->filmpays->removeElement($article);
            if ($film->getPays() === $this){
                $film->setPays(null);  
            }
        }
        return $this;
    }
  
  
  
  
  
  
  
  
  
  
  
    public function _constructP() // Fonction lorsqu'il construit un film
    {
        $this ->personnepays = new ArrayCollection();
    }
  
    public function addPersonnePays(Personne $nationalite): self //Fonction ajout Personne Pays qui permet d'ajouter une personne pour un pays
    {
        if (!$this->personnepays->contains($nationalite)) {
            $this->personnepays[] = $nationalite;
            $nationalite->setNationalite($nationalite);
        }
        return $this;
    }
  
    public function removePersonnePays(Personne $nationalite): self //Fonction supprimer Personne pays qui permet de supprime une personne pour un pays
    {
        if (!$this->personnepays->contains($nationalite)) {
            $this->personnepays->removeElement($nationalite);
            if ($nationalite->getNationalite() === $this){
                $nationalite->setNationalite(null);  
            }
        }
        return $this;
    }
  
    
}
