<?php
//Class Genre.php
    
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Genre")
 */
class Genre {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\Column(name="genre", type="string")
     */
    private $type;
  
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Film", mappedBy="genre")
     */
    private $films;
    
    //Accesseurs
 
    //Getters et setters pour l'objet Id
    function getId(){
        return $this ->id; //Retourne en lecture l'objet Id
    }
    function setId($id){
        $this ->id = $id; //Permet de determiner une valeur pour l'objet Id
    }
      
    //Getters et setters pour l'objet Type
    function getType(){
        return $this ->type; //Retourne en lecture l'objet Type
    }
    function setType($type){
        $this ->type = $type; //Permet de determiner une valeur pour l'objet Type
    }
  
    //Methodes
  
    public function _construct() // Fonction lorsqu'il construit un categorie
    {
        $this ->films = new ArrayCollection();
    }
  
    public function addFilm(Film $film): self //Fonction ajout Film qui permet d'ajouter un film dans un genre
    {
        if (!$this->films->contains($film)) {
            $this->films[] = $film;
            $film->setGenre($film);
        }
        return $this;
    }
  
    public function removeFilm(Film $film): self //Fonction supprimer Film qui permet de supprimer un film dans un genre
    {
        if (!$this->films->contains($film)) {
            $this->films->removeElement($article);
            if ($film->getGenre() === $this){
                $film->setGenre(null);  
            }
        }
        return $this;
    }
  
}

