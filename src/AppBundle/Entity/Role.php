<?php
//Class Role.php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Role")
 */
class Role {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\Column(name="metier", type="string")
     */
    private $metier;
  
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Personne", mappedBy="role")
     */
    private $personnes;
  
    
    //Accesseurs
  
    //Getters et setters pour l'objet Id
    function getId(){
        return $this->id; //Retourne en lecture l'objet Id
    }
    function setId($id){
        $this->id = $id; //Permet de determiner une valeur pour l'objet Id
    }
    
    //Getters et setters pour l'objet Metier
    function getMetier(){
        return $this->metier; //Retourne en lecture l'objet Metier
    }
    function setMetier(){
        $this->metier = $metier; //Permet de determiner une valeur pour l'objet Metier
    }
  
    //methodes
  
    public function _construct() // Fonction lorsqu'il construit un categorie
    {
        $this ->personnes = new ArrayCollection();
    }
  
    public function addPersonne(Personne $personne): self //Fonction ajout Personne qui permet d'ajouter une personne dans un role
    {
        if (!$this->personnes->contains($personne)) {
            $this->personnes[] = $personne;
            $personne->setRole($personne);
        }
        return $this;
    }
  
    public function removePersonne(Personne $personne): self //Fonction supprimer Personne qui permet de supprimer une personne dans un role
    {
        if (!$this->personnes->contains($personne)) {
            $this->personnes->removeElement($personne);
            if ($personne->getRole() === $this){
                $personne->setRole(null);  
            }
        }
        return $this;
    }
}
