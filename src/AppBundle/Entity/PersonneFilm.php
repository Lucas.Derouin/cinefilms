<?php
//Class PersonneFilm.php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Personne_Film")
 */  

class PersonneFilm{
  
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
  
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Personne", inversedBy="personnesfilms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $personne;
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Film", inversedBy="filmspersonnes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $film;
  
    //Getters et setters pour l'objet Id
    public function getId(){
        return $this->id; //Retourne en lecture l'objet Id
    }
    public function setId($id){
        $this->id = $id; //Permet de determiner une valeur pour l'objet Id
    }
  
    //Getters et setters pour l'objet Personne
    public function getPersonne(){
        return $this->personne; //Retourne en lecture l'objet Personne
    }
    public function setPersonne($personne){
        $this->personne = $personne; //Permet de determiner une valeur pour l'objet Personne
    }
  
    //Getters et setters pour l'objet Film
    public function getFilm(){
        return $this->film; //Retourne en lecture l'objet Film
    }
  
    public function setFilm($film)
    {
        $this->film = $film; //Permet de determiner une valeur pour l'objet Film
    }
  
}
  
