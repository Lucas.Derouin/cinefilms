<?php
//Class Utilsateur

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Utilisateur")
 * @UniqueEntity (
 *  fields = {"email"},
 *  message = "L'email que vous avez indiqué est déjà utilisé"
 * )
 */
class Utilisateur implements UserInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="email", type="string", nullable=false, length=255)
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(name="username", type="string", nullable=false, length=255)
     */
    private $username;

    /**
     * @ORM\Column(name="password", type="string", nullable=false, length=255)
     * @Assert\Length(min="8", minMessage="Veuillez saisir 8 caractères minimum")
     * @Assert\EqualTo(propertyPath="confirm_password", message="Vous n'avez pas tapé le même mot de passe")
     */
    private $password;
  
    /**
     * @Assert\EqualTo(propertyPath="password", message="Vous n'avez pas taper le même mot de passe")
     */
    private $confirm_password;
  
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Commentaire", mappedBy="utilisateur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commentaires;
     


    /**
     * Getter et Setter pour Id
     */
    public function getId()
    {
        return $this->id;
    }
    function setId($id){
        $this->id = $id; //Permet de determiner une valeur pour l'objet Id
    }

    /**
     * Getter et Setter pour Email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Getter et Setter pour Username
     */
    function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }
  
    function getUsername()
    {
        return $this->username;
    }

    /**
     * Getter et Setter pour Password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }
  
    /**
     * Getter et Setter pour Password
     */
    public function setConfirmPassword($confirm_password)
    {
        $this->confirm_password = $confirm_password;

        return $this;
    }

    public function getConfirmPassword()
    {
        return $this->confirm_password;
    }
  
    public function eraseCredentials(){
      
    } 
  
    public function getSalt() {
      
    }
  
    public function getRoles() {
        return ['ROLE_USER'];    
    }
  
  
    //methodes 
  
    /*public function __construct() // Fonction 
      {
      $this->commentaires = new ArrayCollection();
      }*/
  
    /**
     * @return Collection|Commentaires[]
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }
  
    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->addUtilisateur($this);
        }
        return $this;
    }
  
    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            $commentaire->removeCommentaire($this);
        }
        return $this;
    }
}

