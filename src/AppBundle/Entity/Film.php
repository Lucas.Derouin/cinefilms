<?php
//Class Film.php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;



/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Film")
 */  

class Film{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\Column(name="titre", type="string", nullable=false)
     */
    private $titre;
    
    /** 
     * @ORM\Column(name="annee", type="integer", nullable=false)
     */
    private $annee;
  
    /**
     * @ORM\Column(name="image", type="string", nullable=false)
     * @Assert\Url()
     */
    private $image;
    
    /**
     * @ORM\Column(name="synopsis", type="text", nullable=false)
     */
    private $synopsis;
    
    /**
     * @ORM\Column(name="duree", type="integer", nullable=false)
     */
    private $duree;
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Genre", inversedBy="films")
     * @ORM\JoinColumn(nullable=false)
     */
    private $genre;
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays", inversedBy="filmpays")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pays;
  
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PersonneFilm", mappedBy="film")
     */
    private $filmspersonnes;
  
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Commentaire", mappedBy="film")
     */
    private $commentaires;
  
  
  
    public function __construct() // Fonction 
    {
        $this->commentaires = new ArrayCollection();
        $this->filmspersonnes = new ArrayCollection();
    }
    //Accesseurs
  
    //Getters et setters pour l'objet Id
    public function getId(){
        return $this->id; //Retourne en lecture l'objet Id
    }
    public function setId($id){
        $this->id = $id; //Permet de determiner une valeur pour l'objet Id
    }

    //Getters et setters pour l'objet Titre
    public function getTitre(){
        return $this->titre; //Retourne en lecture l'objet Titre
    }
    public function setTitre($titre){
        $this->titre = $titre; //Permet de determiner une valeur pour l'objet Titre
    }

    //Getters et setters pour l'objet Annee
    public function getAnnee(){
        return $this ->annee; //Retourne en lecture l'objet Annee
    }
    public function setAnnee($annee){
        $this->annee = $annee; //Permet de determiner une valeur pour l'objet Annee
    }
  
    //Getters et setters pour l'objet Image
    public function getImage(){
        return $this ->image; //Retourne en lecture l'objet Image
    }
    public function setImage($image){
        $this->image = $image; //Permet de determiner une valeur pour l'objet Image
    }

    //Getters et setters pour l'objet Synopsis
    function getSynopsis(){
        return $this ->synopsis; //Retourne en lecture l'objet Synopsis
    }
    function setSynopsis($synopsis){
        $this->synopsis = $synopsis; //Permet de determiner une valeur pour l'objet Synopsis
    }

    //Getters et setters pour l'objet Duree
    function getDuree(){
        return $this->duree; //Retourne en lecture l'objet Duree
    }
    function setDuree($duree){
        $this->duree = $duree; //Permet de determiner une valeur pour l'objet Duree
    }
  
    //Getters et setters pour l'objet Genre
    public function getGenre(){
        return $this->genre; //Retourne en lecture l'objet Genre
    }
    public function setGenre($genre){
        $this->genre = $genre; //Permet de determiner une valeur pour l'objet Genre
    }
  
    //Getters et setters pour l'objet Pays
    public function getPays()
    {
        return $this->pays; //Retourne en lecture l'objet Pays
    }

    public function setPays($pays)
    {
        $this->pays = $pays; //Permet de determiner une valeur pour l'objet Pays
    }

        
    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaire() : Collection
    {
        return $this->commentaires;
    }

    //Fonction ajout Film qui permet d'ajouter un Commentaire dans un film
    public function addCommentaire(Commentaire $commentaire): self 
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setFilm($commentaire);
        }
        return $this;
    }
    //Fonction supprimer Film qui permet de supprimer un commetnaire dans un film
    public function removeCommentaire(Commentaire $commentaire): self 
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            if ($commentaire->getFilm() === $this){
                $commentaire->setFilm(null);  
            }
        }
        return $this;
    }
  
    //methodes 
  
    /**
     * @return Collection|FilmsPersonnes[]
     */
    public function getFilmsPersonnes(): Collection 
    {
        return $this->filmspersonnes;
    }
    
    function setFilmsPersonnes($filmspersonnes) {
        $this->filmspersonnes = $filmspersonnes;
        return $this;
    }
  
  
    public function addFilmsPersonnes(PersonneFilm $filmpersonne): self
    {
        if (!$this->filmspersonnes->contains($filmpersonne)) {
            $this->filmspersonnes[] = $filmpersonne;
            $filmpersonne->setFilm($filmpersonne);
        }
        return $this;
    }
  
    public function removeFilmsPersonnes(PersonneFilm $filmpersonne): self
    {
        if ($this->filmspersonnes->contains($filmpersonne)) {
            $this->filmspersonnes->removeElement($filmpersonne);
            if ($filmpersonne->getFilm() === $this){
                $filmpersonne->setFilm(null);  
            } 
        }
        return $this;
    }
    
}
