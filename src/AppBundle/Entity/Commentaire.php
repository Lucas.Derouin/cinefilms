<?php
//Class Commentaire

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Commentaire")
 */
class Commentaire
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
  
    /**
     * @ORM\Column(name="contenu", type="text", nullable=false)
     */
    private $contenu;

    /**
     * @ORM\Column(name="datecreation", type="datetime", nullable=false)
     */
    private $datecreation;
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Film", inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $film;
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Utilisateur", inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;
     
    


    /**
     * Getter et Setter pour Id
     */
    public function getId()
    {
        return $this->id;
    }
    function setId($id){
        $this->id = $id; //Permet de determiner une valeur pour l'objet Id
    }
  
    /**
     * Getter et Setter pour Contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Getter et Setter pour DateCreation
     */
    public function setDateCreation($datecreation)
    {
        $this->datecreation = $datecreation;

        return $this;
    }
  
    public function getDateCreation()
    {
        return $this->datecreation;
    }
  
    /**
     * Getter et Setter pour Film
     */
    public function setFilm($film)
    {
        $this->film = $film;

        return $this;
    }
  
    public function getFilm()
    {
        return $this->film;
    }
  
    /**
     * Getter et Setter pour Utilisateur
     */
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
  
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
    

}
