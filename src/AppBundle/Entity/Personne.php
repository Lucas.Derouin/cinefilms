<?php
// Class Personne.php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="cinefilms.Personne")
 */
class Personne {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\Column(name="nom", type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\Column(name="prenom", type="string")
     */
    private $prenom;
  
    /**
     * @ORM\Column(name="image", type="string", nullable=false)
     * @Assert\Url()
     */
    private $image;
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Role", inversedBy="personnes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $role;
  
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays", inversedBy="payspersonne")
     * @ORM\JoinColumn(nullable=false)
     */
    private $nationalite;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PersonneFilm", mappedBy="personne")
     */
    private $personnesfilms;
  
  

    //Accesseurs
  
    public function __construct()
    {
        $this->personnesfilms = new ArrayCollection();
    }
  
    //Getters et setters pour l'objet Id
    public function getId(){
        return $this->id; //Retourne en lecture l'objet Id
    }
    public function setId($id){
        $this->id = $id; //Permet de determiner une valeur pour l'objet Matricule
    }
    
    //Getters et setters pour l'objet Nom
    public function getNom(){
        return $this->nom; //Retourne en lecture l'objet Nom
    }
    public function setNom($nom){
        $this->nom = $nom; //Permet de determiner une valeur pour l'objet Nom
    }

    //Getters et setters pour l'objet Prenom
    public function getPrenom(){
        return $this->prenom; //Retourne en lecture l'objet Prenom
    }
    public function setPrenom($prenom){
        $this->prenom = $prenom; //Permet de determiner une valeur pour l'objet Prenom
    }
  
    //Getters et setters pour l'objet Image
    public function getImage(){
        return $this ->image; //Retourne en lecture l'objet Image
    }
    public function setImage($image){
        $this->image = $image; //Permet de determiner une valeur pour l'objet Image
    }

    //Getters et setters pour l'objet Nationalite
    public function getNationalite(){
        return $this->nationalite; //Retourne en lecture l'objet Nationalite
    }
    public function setNationalite($nationalite){
        $this->nationalite = $nationalite; //Permet de determiner une valeur pour l'objet Nationalite
    }
  
    //Getters et setters pour l'objet Role
    public function getRole(){
        return $this->role; //Retourne en lecture l'objet Role
    }
    public function setRole($role){
        $this->role = $role; //Permet de determiner une valeur pour l'objet Role
    }
  
  
  
    /**
     * @return Collection|PersonnesFilms[]
     */
    public function getPersonnesFilms(): Collection
    {
        return $this->personnesfilms;
    }
  
    public function addPersonnesFilms(PersonneFilm $personnefilm): self
    {
        if (!$this->personnesfilms->contains($personnefilm)) {
            $this->personnesfilms[] = $personnefilm;
            $personnefilm->setPersonne($personnefilm);
        }
        return $this;
    }
  
    public function removePersonnesFilm(PersonneFilm $personnefilm): self
    {
        if ($this->personnesfilms->contains($personnefilm)) {
            $this->personnesfilms->removeElement($personnefilm);
            if ($personnefilm->getPersonne() === $this){
                $personnefilm->setPersonne(null);  
            } 
        }
            
        return $this;
    }

}
